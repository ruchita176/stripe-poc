import constant from "../config/constant";
import PaymentStrategy from "./paymentStrategy";

// This is a sample test API key.
const stripe = require("stripe")(constant.stripeSecretKey);

export default class Stripe extends PaymentStrategy {
  async createPaymentIntent() {
    try {
      const paymentIntent = await stripe.paymentIntents.create({
        amount: constant.price,
        currency: "inr",
        payment_method_types: ["card"],
      });
      console.log(paymentIntent);
      return paymentIntent;
    } catch (err) {
      console.log("error", err);
      return err;
    }
  }

  async createCheckoutSession() {
    try {
      const session = await stripe.checkout.sessions.create({
        line_items: [
          {
            price_data: {
              currency: "inr",
              product_data: {
                name: "CC POC",
              },
              unit_amount: 2400,
            },
            quantity: 1,
          },
        ],
        payment_method_types: ["card"],
        mode: "payment",
        success_url: `https://intense-castle-91784.herokuapp.com/success`,
        cancel_url: `https://intense-castle-91784.herokuapp.com/cancel`,
      });
      console.log("session", session);
      return session;
    } catch (err) {
      console.log("err", err);
      return err;
    }
  }

  async productCheckoutSession() {
    try {
      const session = await stripe.checkout.sessions.create({
        mode: "subscription",
        payment_method_types: ["card"],
        line_items: [
          {
            price: this.priceId,
            // For metered billing, do not pass quantity
            quantity: 1,
          },
        ],
        // {CHECKOUT_SESSION_ID} is a string literal; do not change it!
        // the actual Session ID is returned in the query parameter when your customer
        // is redirected to the success page.
        success_url: `https://intense-castle-91784.herokuapp.com/success?session_id={CHECKOUT_SESSION_ID}`,
        cancel_url: `https://intense-castle-91784.herokuapp.com/cancel`,
      });
      console.log("session", session);
      return session;
    } catch (err) {
      console.log("err", err);
      return err;
    }
  }
}
