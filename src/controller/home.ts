import moment from "moment";
import models from "../models/stripePOCModels";
import CPayment from '../context/CPayment';
import Stripe from './stripe';
let Transaction = models.Transaction;
let RecurringInvoice = models.RecurringInvoice;
import Logger from '../loaders/logger';

export class HomeController {
  static async createPayment() {
    try {
      const payment = new CPayment();
      // Create a PaymentIntent with the order amount and currency
      const paymentIntent = await payment.createPaymentIntent(new Stripe(null));
      Logger.info(JSON.parse(paymentIntent));
      return paymentIntent;
    } catch (error) {
      Logger.error(error);
      return false;
    }
  }

  static async createCheckoutSession() {
    try {
      const payment = new CPayment();
      const session = await payment.createCheckoutSession(new Stripe(null));
      Logger.info(session);
      return session;
    } catch (error) {
      Logger.error(error);
      return false;
    }
  }

  static async productCheckout(req) {
    const priceId = req.body.priceId;
    try {
      const payment = new CPayment();
      const session = await payment.productCheckoutSession(new Stripe(priceId));
      Logger.info(session);
      return session;
    } catch (e) {
      Logger.error(e);
      return false;
    }
  }

  static async productRecurringWebhook(req) {
    const data = req.body.data.object;
    const eventType = req.body.type;
    Logger.info(req.body.type);
    console.log(data, "data");
    switch (eventType) {
      case "checkout.session.completed":
        // Payment is successful and the subscription is created.
        // You should provision the subscription and save the customer ID to your database.
        var newTransaction = new Transaction();
        newTransaction.amount_subtotal = data.amount_subtotal;
        newTransaction.amount_total = data.amount_total;
        newTransaction.client_reference_id = data.client_reference_id;
        newTransaction.currency = data.currency;
        newTransaction.customer = data.customer;
        newTransaction.customer_details = data.customer_details;
        newTransaction.customer_email = data.customer_email;
        newTransaction.mode = data.mode;
        newTransaction.payment_intent_id = data.payment_intent_id;
        newTransaction.payment_status = data.payment_status;
        newTransaction.subscription = data.subscription;
        newTransaction.created_date = parseInt(moment().format("X"));
        newTransaction.save();
        break;
      // case "invoice.paid":
      //   // Continue to provision the subscription as payments continue to be made.
      //   // Store the status in your database and check when a user accesses your service.
      //   // This approach helps you avoid hitting rate limits.
      //   var newRecurring = new RecurringInvoice();
      //   newRecurring.account_country = data.account_country;
      //   newRecurring.account_name = data.account_name;
      //   newRecurring.amount_due = data.amount_due;
      //   newRecurring.amount_paid = data.amount_paid;
      //   newRecurring.amount_remaining = data.amount_remaining;
      //   newRecurring.charge_id = data.charge;
      //   newRecurring.currency = data.currency;
      //   newRecurring.customer = data.customer;
      //   newRecurring.customer_email = data.customer_email;
      //   newRecurring.customer_name = data.customer_name;
      //   newRecurring.next_payment_attempt = data.next_payment_attempt;
      //   newRecurring.tax = data.tax;
      //   newRecurring.total = data.total;
      //   newRecurring.subscription = data.subscription;
      //   newRecurring.created_date = parseInt(moment().format("X"));
      //   newRecurring.save();
      //   break;

      case "customer.subscription.updated":
        // Continue to provision the subscription as payments continue to be made.
        // Store the status in your database and check when a user accesses your service.
        // This approach helps you avoid hitting rate limits.
        var newRecurring = new RecurringInvoice();
        newRecurring.account_country = data.account_country;
        newRecurring.account_name = data.account_name;
        newRecurring.amount_due = data.amount_due;
        newRecurring.amount_paid = data.amount_paid;
        newRecurring.amount_remaining = data.amount_remaining;
        newRecurring.charge_id = data.charge;
        newRecurring.currency = data.currency;
        newRecurring.customer = data.customer;
        newRecurring.customer_email = data.customer_email;
        newRecurring.customer_name = data.customer_name;
        newRecurring.next_payment_attempt = data.next_payment_attempt;
        newRecurring.tax = data.tax;
        newRecurring.total = data.total;
        newRecurring.subscription = data.subscription;
        newRecurring.created_date = parseInt(moment().format("X"));
        newRecurring.save();
        break;
      
      
      case "invoice.payment_failed":
        // The payment failed or the customer does not have a valid payment method.
        // The subscription becomes past_due. Notify your customer and send them to the
        // customer portal to update their payment information.
        //In future we can integrate mailer service here
        break;
      default:
      // Unhandled event type
    }
    return { status: 200, message: "DB updated successfully" };
  }
}
