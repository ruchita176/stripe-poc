import winston from 'winston';
import {ElasticsearchTransport, ElasticsearchTransformer} from 'winston-elasticsearch';

const currentTime = new Date().getTime();

console.log("Current time", currentTime);

const esTransportOpts = {
  level: 'info',
  clientOpts: { node: 'http://localhost:9200', log: 'info' },
  transformer: logData => {
    return {
      "@timestamp": currentTime,
      severity: logData.level,
      message: `[${logData.level}] LOG Message: ${logData.message}`,
      fields: {}
    }
  }
};
console.log("esTransportOpts", esTransportOpts);
const esTransport = new ElasticsearchTransport(esTransportOpts);

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  transports: [
    new winston.transports.File({ filename: "logfile.log", level: 'error' }), //save errors on file
    new winston.transports.File({ filename: "logfileinfo.log", level: 'info' }), //save info on file
    esTransport
  ]
});

logger.add(new winston.transports.Console({ //we also log to console if we're not in production
  format: winston.format.simple()
}));

logger.on('error', (error) => {
  console.error('Error in logger caught', error);
});
esTransport.on('error', (error) => {
  console.error('Error in logger caught', error);
});

export default logger;