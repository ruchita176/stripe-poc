import expressLoader from './express';
import dbLoader from './database';

export default async ({ expressApp }) => {
	// Establish a database connection for node's process
	dbLoader()
		.then(response => {
			console.log('✌️ Connected to database');
		})
		.catch(err => {
			console.log('Could not connect to database');
			console.log(err.message);
		});

	console.log('✌️ Express loaded');
	await expressLoader({ app: expressApp });
};
