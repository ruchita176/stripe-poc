/***************Mongodb configuratrion********************/
import MongoClient from 'mongoose';
import configDB from '../config/database';
//configuration ===============================================================


export default async () => {
	// create the connection to database
	await MongoClient.connect(configDB.url, {useNewUrlParser: true, useUnifiedTopology: true});
};
