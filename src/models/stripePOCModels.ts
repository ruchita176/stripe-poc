var mongoose = require("mongoose");
var Schema = mongoose.Schema;

//Product schema
var productSchema = mongoose.Schema({
  id: Number,
  product_name: { type: String, default: "" },
  product_desc: { type: String, default: "" },
  stripe_id: { type: String, default: "" },
  status: { type: Number, default: 0 },
  created_date: String,
  updated_date: String
});
var Product = mongoose.model("Product", productSchema);

//Product Transaction Schema
var transactionSchema = mongoose.Schema({
  amount_subtotal: { type: Number, default: 0 },
  amount_total: { type: Number, default: 0 },
  client_reference_id: { type: String, default: "" },
  currency: { type: String, default: "" },
  customer: { type: String, default: "" },
  customer_details: { type: Object, default: null },
  customer_email: { type: String, default: "" },
  mode: { type: String, default: "" },
  payment_intent_id: { type: String, default: "" },
  payment_status: { type: String, default: "" },
  subscription: { type: String, default: "" },
  created_date: String,
  updated_date: String,
});
var Transaction = mongoose.model("Transaction", transactionSchema);

//Recurring Invoice Schema
var recurringInvoiceScema = mongoose.Schema({
  account_country: { type: String, default: "" },
  account_name: { type: String, default: "" },
  amount_due: { type: Number, default: 0 },
  amount_paid: { type: Number, default: 0 },
  amount_remaining: { type: Number, default: 0 },
  charge_id: { type: String, default: "" },
  currency: { type: String, default: "" },
  customer: { type: String, default: "" },
  customer_email: { type: String, default: "" },
  customer_name: { type: String, default: "" },
  next_payment_attempt: { type: String, default: "" },
  tax: { type: String, default: "" },
  total: { type: String, default: "" },
  subscription: { type: String, default: "" },
  created_date: String,
  updated_date: String,
});
var RecurringInvoice = mongoose.model("RecurringInvoice", recurringInvoiceScema);

export default {
  Product: Product,
  Transaction: Transaction,
  RecurringInvoice: RecurringInvoice
};
