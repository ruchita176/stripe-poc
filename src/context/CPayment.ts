//Context
export default class CPayment {
    createPaymentIntent(processor) {
        return processor.createPaymentIntent();
    }
    createCheckoutSession(processor) {
        return processor.createCheckoutSession();
    }
    productCheckoutSession(processor) {
        return processor.productCheckoutSession();
    }
}
