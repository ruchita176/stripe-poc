import { Router } from 'express';
import payment from './routes/payment';

// guaranteed to get dependencies
export default () => {
	const app = Router();
	payment(app);
	return app;
};