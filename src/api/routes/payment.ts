import { Router } from 'express';
import constant from '../../config/constant';
import logger from '../../loaders/logger';
import { HomeController } from '../../controller/home';

const route = Router();

export default (app) => {
	app.use('/', route);

	//all Routes of '/'
    route.get('/', showIndexPage);
    route.get('/success', showSuccessPage);
    route.get('/cancel', showCancelPage);
    route.get('/cards', showCardsPage);
    route.post('/create-payment-intent', createPayment);
    route.post('/create-checkout-session', createCheckoutSession);
    route.get('/checkout', showProductCheckoutPage);
    route.post('/product-checkout', productCheckout);
    route.post('/webhook', productRecurringWebhook);
};

async function showIndexPage(req, res) {
	try {
        logger.info("This is test err");
        res.render("checkout");
    } catch (error) {
        res.render("error");
    }
}

async function showSuccessPage(req, res) {
	try {
        res.render("success");
    } catch (error) {
        res.render("error");
    }
}

async function showCancelPage(req, res) {
	try {
        res.render("cancel");
    } catch (error) {
        res.render("error");
    }
}

async function showCardsPage(req, res) {
	try {
        res.render("cards", {
          key: constant.stripePublicKey,
        });
    } catch (error) {
        res.render("error");
    }
}

async function showProductCheckoutPage(req, res) {
	try {
        res.render("product-checkout");
    } catch (error) {
        res.render("error");
    }
}

async function createPayment(req, res) {
	HomeController.createPayment()
		.then(response => {
			if (response) {
				res.send({
                    clientSecret: response.client_secret,
                  });
			} else {
                res.render("error");
            }
		})
		.catch(e => {
			res.render("error");
		});
}

async function createCheckoutSession(req, res) {
	HomeController.createCheckoutSession()
		.then(response => {
			if (response) {
				res.redirect(303, response.url);
			} else {
                res.render("error");
            }
		})
		.catch(e => {
			res.render("error");
		});
}

async function productCheckout(req, res) {
	HomeController.productCheckout(req)
		.then(response => {
			if (response) {
                console.log(response);
				res.redirect(303, response.url);
			} else {
                res.render("error");
            }
		})
		.catch(e => {
			res.render("error");
		});
}

async function productRecurringWebhook(req, res) {
	HomeController.productRecurringWebhook(req)
    .then(response => {
        if (response) {
            return res.status(200).json(response);
        } else {
            return res.status(400).json('Something went wrong');
        }
    })
    .catch(e => {
        console.log(e);
        return res.status(500).end();
    });
}